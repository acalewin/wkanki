require 'json'
class Sentence < ActiveRecord::Base

  validates :content, presence: true

  belongs_to :user
  has_many :sentence_kanji
  has_many :kanji, :through => :sentence_kanji
  has_one :text_sentence
  has_one :text, :through => :text_sentence

  after_save :queue_parse, on: [:create]

  def queue_parse
    self.delay.parse unless self.parsed?
    self.delay.decompose unless self.decomposition
  end

  def parse
    ks = Hash.new(0)
    self.content.chars.find_all { |c| /[\u4e00-\u9faf]/.match(c)   }.each do |k|
      kanji = Kanji.find_by(:character => k)
      if kanji.nil?
        kanji = Kanji.create(:character => k)
      end
      uk = self.user.user_kanji.find_by(:kanji => kanji)
      if uk.nil?
        uk = self.user.user_kanji.create(:kanji => kanji, :srs => 'unknown', :source => 'other')
      end
      ks[uk.srs] += 1
      sk = self.sentence_kanji.find_by(:kanji => kanji)
      if sk.nil?
        self.sentence_kanji.create(:kanji=> kanji)
      end
    end
    self.score_kanji = JSON.generate(ks)
    self.parsed = true
    self.save
  end

  def decompose
    require 'natto'
    self.decomposition = JSON.generate(Natto::MeCab.new.parse(self.content).split("\n").map { |t| t.split(/[,\t]/)  }.reject { |t| t[0] == 'EOS'  }.map { |f| {:original => f[0], :pos => f[1], :conjugated => f[6], :reading=>f[8], :pronunciation =>f[9]}  })
    self.save
  end

  def kanji_score
    score = Hash.new(0)
    if self.score_kanji
      return JSON.parse(self.score_kanji)
    end
    self.kanji.each do |k|
      kanji = self.user.user_kanji.find_by(:kanji => k)
      score[kanji.srs] += 1
    end
    self.score_kanji = JSON.generate(score)
    self.save
    score
  end
end
