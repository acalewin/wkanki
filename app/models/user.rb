class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :user_vocab
  has_many :vocabulary, :through => :user_vocab
  has_many :user_kanji
  has_many :kanji, :through => :user_kanji
  has_many :sentence
  has_many :text

  def load_kanji
    require 'kwabby/kwabby'
    if !self.apikey
      return nil
    end
    wk = Wanikani.new(self.apikey)
    wk.kanji.each do |k|
      srs_stats = k.fetch('user_specific', nil)
      if srs_stats.nil?
        next
      end
      kanji = self.kanji.find_by(character: k.fetch('character'))
      if !kanji
        kanji = self.kanji.new(k.reject { |k,v| ['user_specific'].include? k })
        kanji.save()
      end
      uk = self.user_kanji.find_by(kanji: kanji)
      uk.srs = srs_stats['srs']
      uk.source = 'wanikani'
      uk.save()
   end
    self.sentence.all.update_all(parsed: false)
  end

  def load_vocab
    require 'kwabby/kwabby'
    if !self.apikey
      return nil
    end
    wk = Wanikani.new(self.apikey)
    5.times do |m| 
        wk.vocabulary((1..10).to_a.map { |y| y*(m+1)}.join(',')).each do |k|
          srs_stats = k.fetch('user_specific', nil)
      if srs_stats.nil?
        next
      end
      vocabulary = self.vocabulary.find_by(character: k.fetch('character'))
      if !vocabulary
        vocabulary = self.vocabulary.new(k.reject { |k,v| ['user_specific'].include? k })
        vocabulary.save()
      end
      uk = self.user_vocab.find_by(vocabulary: vocabulary)
      uk.srs = srs_stats['srs']
   #   uk.source = 'wanikani'
      uk.save()
   end
    end
    self.sentence.all.update_all(parsed: false)
  end
end
