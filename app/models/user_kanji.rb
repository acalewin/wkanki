class UserKanji < ActiveRecord::Base
  belongs_to :user
  belongs_to :kanji
end
