class SentenceVocab < ActiveRecord::Base
  belongs_to :vocabulary
  belongs_to :sentence
end
