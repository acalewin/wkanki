class Vocabulary < ActiveRecord::Base
    
  has_many :user_vocab
  has_many :sentence_vocabulary
  has_many :sentence, :through => :sentence_vocabulary
end
