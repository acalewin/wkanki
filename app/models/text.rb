class Text < ActiveRecord::Base
  has_attached_file :document
  validates_attachment_content_type :document, :content_type => /\Atext\/.*\Z/
  validates :name, presence: true

  belongs_to :user
  has_many :text_sentence
  has_many :sentence, :through => :text_sentence

  after_save :queue_parse, on: [:create]

  def queue_parse
    self.delay.parse
  end

  def parse
    File.open(self.document.path, 'r') do |f|
      while line = f.gets
        line.chomp!
        if line.empty?
          next
        end
        self.sentence.create(:user => self.user, :content => line)
      end
    end
  end

  def length
    self.sentence.inject(0) { |sum, s| sum + s.content.length  }
  end

  def kanji_score
    score = Hash.new(0)
    self.sentence.each do |s|
      ks = s.kanji_score
      if ks
        s.kanji_score.keys.each { |k| score[k] += s.kanji_score.fetch(k, 0) }
      end
    end
    score
  end
end
