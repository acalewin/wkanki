class SentenceKanji < ActiveRecord::Base
  belongs_to :kanji
  belongs_to :sentence
end
