class Kanji < ActiveRecord::Base

  validates :character, presence: true

  has_many :user_kanji
  has_many :sentence_kanji
  has_many :sentence, :through => :sentence_kanji
end
