class DashboardController < ApplicationController
  before_action :authenticate_user!

  def summary
    @text = Text.new
    require 'kwabby/kwabby'
    @study_queue = Wanikani.new(current_user.apikey).study_queue
  end
end
