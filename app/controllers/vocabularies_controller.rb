class VocabulariesController < ApplicationController

  before_action :authenticate_user!

  def index
    if current_user.vocabulary.length == 0
      if !current_user.apikey
        redirect_to current_user
      end
      current_user.delay.load_vocab
    end
    @user_vocabs = current_user.user_vocab.paginate(:page => params[:page])
  end

  def show
    begin
      @vocabulary = Vocabulary.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      redirect_to root_path
    end
    @user_srs = current_user.user_vocab.find_by(:vocabulary => @vocabulary)
  end

  def refresh
    if !current_user.apikey
      redirect_to current_user
    end
    current_user.delay.load_vocab
    redirect_to vocabularies_path
  end
end
