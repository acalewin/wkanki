class TextsController < ApplicationController
  def new
    @text = Text.new
  end

  def index
    @texts = current_user.text
  end

  def create
    @text = current_user.text.new
    text_params = secure_params
    @text.name = text_params[:name]
    if text_params[:content]
      @text.save
      text_params[:content].lines.each do |l|
        @text.sentence.create(:user => current_user, :content => l.chomp)
      end
    else
      @text.document = text_params[:document]
    end
#    @text = current_user.text.create(secure_params)
    if @text.valid?
      flash[:notice] = "Text saved."
      @text.save
    else
      redirect_to :back
    end
    redirect_to texts_path
  end

  def show
    @text = current_user.text.find(params[:id])
  end

  private
  def secure_params
    params.require(:text).permit(:document, :name, :content)
  end
end
