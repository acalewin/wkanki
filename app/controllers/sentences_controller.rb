class SentencesController < ApplicationController
  before_action :authenticate_user!

  def new
    @sentence = Sentence.new
  end

  def index
    @sentence = Sentence.new
    @sentences = current_user.sentence.paginate(:page => params[:page])
  end

  def create
    @sentence = current_user.sentence.new(sentence_params)
    if @sentence.valid?
      flash[:notice] = "Sentence saved."
      @sentence.save
      redirect_to sentences_path
    else
      redirect_to :back
    end
  end

  def show
    @sentence = Sentence.find(params[:id])
  end

  private

  def sentence_params
    params.require(:sentence).permit(:content)
  end
end
