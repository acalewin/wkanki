class KanjisController < ApplicationController

  before_action :authenticate_user!

  def index
    if current_user.kanji.length == 0
      if !current_user.apikey
        redirect_to current_user
      end
      current_user.delay.load_kanji
    end
    @user_kanjis = current_user.user_kanji.paginate(:page => params[:page])
  end

  def show
    begin
      @kanji = Kanji.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      redirect_to root_path
    end
    @user_srs = current_user.user_kanji.find_by(:kanji => @kanji)
  end

  def refresh
    if !current_user.apikey
      redirect_to current_user
    end
    current_user.delay.load_kanji
    redirect_to kanjis_path
  end
end
