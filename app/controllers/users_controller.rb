class UsersController < ApplicationController

  before_action :authenticate_user!

  def new
    @user = User.new
  end

  def update
    @user = current_user
    Rails.logger.debug('I got an update for a user')
    @user.update(secure_params)
    session[:user_key] = @user.apikey
    redirect_to root_path
  end

  def show
    @user = current_user
    if @user.valid?
      #get a list of kanji from WK
      Rails.logger.debug("I got a request from user #{@user.apikey}")
    else
      render :new
    end
  end

  private

  def secure_params
    params.require(:user).permit(:apikey)
  end
end
