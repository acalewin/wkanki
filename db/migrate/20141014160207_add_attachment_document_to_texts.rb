class AddAttachmentDocumentToTexts < ActiveRecord::Migration
  def self.up
    change_table :texts do |t|
      t.attachment :document
    end
  end

  def self.down
    remove_attachment :texts, :document
  end
end
