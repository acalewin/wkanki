class AddScoreKanjiToSentence < ActiveRecord::Migration
  def change
    add_column :sentences, :score_kanji, :string
  end
end
