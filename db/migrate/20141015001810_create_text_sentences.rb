class CreateTextSentences < ActiveRecord::Migration
  def change
    create_table :text_sentences do |t|
      t.integer :text_id
      t.integer :sentence_id

      t.timestamps
    end
  end
end
