class CreateSentenceVocabs < ActiveRecord::Migration
  def change
    create_table :sentence_vocabs do |t|
      t.integer :sentence_id
      t.integer :vocabulary_id

      t.timestamps
    end
  end
end
