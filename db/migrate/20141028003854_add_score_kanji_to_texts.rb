class AddScoreKanjiToTexts < ActiveRecord::Migration
  def change
    add_column :texts, :score_kanji, :string
  end
end
