class CreateSentences < ActiveRecord::Migration
  def change
    create_table :sentences do |t|
      t.text :content, :limit => nil
      t.integer :user_id
      t.boolean :parsed

      t.timestamps
    end
  end
end
