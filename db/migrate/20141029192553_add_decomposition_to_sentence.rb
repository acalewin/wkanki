class AddDecompositionToSentence < ActiveRecord::Migration
  def change
    add_column :sentences, :decomposition, :string
  end
end
