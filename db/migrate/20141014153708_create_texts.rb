class CreateTexts < ActiveRecord::Migration
  def change
    create_table :texts do |t|
      t.string :name
      t.integer :user_id

      t.timestamps
    end
  end
end
