class CreateSentenceKanjis < ActiveRecord::Migration
  def change
    create_table :sentence_kanjis do |t|
      t.integer :kanji_id
      t.integer :sentence_id

      t.timestamps
    end
  end
end
