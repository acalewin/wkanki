class CreateUserVocabs < ActiveRecord::Migration
  def change
    create_table :user_vocabs do |t|
      t.integer :user_id
      t.integer :vocabulary_id
      t.string :srs
      t.string :source
      t.timestamps
    end
  end
end
