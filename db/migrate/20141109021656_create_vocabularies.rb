class CreateVocabularies < ActiveRecord::Migration
  def change
    create_table :vocabularies do |t|
      t.string :character
      t.string :kana
      t.string :meaning
      t.integer :level

      t.timestamps
    end
  end
end
