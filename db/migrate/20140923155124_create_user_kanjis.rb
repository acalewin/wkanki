class CreateUserKanjis < ActiveRecord::Migration
  def change
    create_table :user_kanjis do |t|
      t.string :srs
      t.integer :user_id
      t.integer :kanji_id
      t.string :source

      t.timestamps
    end
  end
end
