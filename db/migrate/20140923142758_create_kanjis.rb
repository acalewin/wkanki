class CreateKanjis < ActiveRecord::Migration
  def change
    create_table :kanjis do |t|
      t.string :character
      t.string :onyomi
      t.string :kunyomi
      t.string :meaning
      t.integer :level
      t.string :important_reading

      t.timestamps
    end
  end
end
